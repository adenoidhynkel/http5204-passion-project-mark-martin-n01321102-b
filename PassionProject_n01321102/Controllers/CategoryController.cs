﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Net;
using System.Data;
using System.Data.Entity;
using System.Data.SqlClient;
using PassionProject_n01321102.Models;

namespace PassionProject_n01321102.Controllers
{
    public class CategoryController : Controller
    {

        //database object
        TeamBuilderContext database = new TeamBuilderContext();

        // GET: Category
        public ActionResult Index()
        {
            return RedirectToAction("List");
        }

        public ActionResult List()
        {
            //IEnumerable<Category> cat = database.Categories.ToList();
            string query = "SELECT * FROM Categories";
            IEnumerable<Category> cat = database.Categories.SqlQuery(query);

            return View(cat);
        }

        public ActionResult Add()
        {
            return View();
        }

        [HttpPost]
        public ActionResult AddCategory(string New_CategoryName, string New_CategoryDescription)
        {
            string addQuery = "INSERT INTO Categories (CategoryName, CategoryDescription) VALUES (@catName, @catDesc)";

            // create a SQL param array
            // push values to the array
            // I find this is much cleaner than specifying the index number
            // source: https://stackoverflow.com/questions/44722689/no-mapping-exists-from-object-type-system-collections-generic-list
            // author: Alex
            SqlParameter[] sqlValues = new SqlParameter[2] 
            {
                new SqlParameter("@catName", SqlDbType.VarChar) { Value = New_CategoryName},
                new SqlParameter("@catDesc", SqlDbType.VarChar) { Value = New_CategoryDescription}
            };


            //execute SQL Query
            database.Database.ExecuteSqlCommand(addQuery, sqlValues);
            
            //redirect to Category List
            return RedirectToAction("List");
        }

        // View edit page
        public ActionResult Edit(int? id)
        {
            // check if the id is null or not existing
            // I copied this code from Christine
            if ((id == null) || (database.Categories.Find(id) == null))
            {
                return HttpNotFound();
            }

            string editQuery = "SELECT * FROM Categories WHERE CategoryID = @id";

            // Create a new sql Param
            // it's not initialized as an array because we only have one parameter
            SqlParameter sqlValue = new SqlParameter("@id", id);
            
            
            // I copied this code from Christine
            return View(database.Categories.SqlQuery(editQuery, sqlValue).FirstOrDefault());
        }

        // saving the edited values from edit page
        [HttpPost]
        public ActionResult Modify(int? id, string CategoryName, string CategoryDescription)
        {
            // check if the id is null or not existing
            // I copied this code from Christine
            if ((id == null) || (database.Categories.Find(id) == null))
            {
                return HttpNotFound();
            }

            string modifyQuery = "UPDATE Categories SET CategoryName = @catName, CategoryDescription = @catDesc WHERE CategoryID = @catID";

            // create an SQL Parameter array
            // and push values to array
            SqlParameter[] sqlValues = new SqlParameter[3]
            {
                new SqlParameter("@catName", SqlDbType.VarChar) { Value = CategoryName},
                new SqlParameter("@catDesc", SqlDbType.VarChar) { Value = CategoryDescription},
                new SqlParameter("@catID", SqlDbType.Int) { Value = id}
            };

            database.Database.ExecuteSqlCommand(modifyQuery, sqlValues);

            return RedirectToAction("Details/" + id);
        }

        // deleting a category
        public ActionResult Delete(int id)
        {
            string deleteQuery = "DELETE FROM Categories WHERE CategoryID = @catID";
            SqlParameter sqlValue = new SqlParameter("@catID", id);

            database.Database.ExecuteSqlCommand(deleteQuery, sqlValue);

            return RedirectToAction("List");
        }

        // Show a single category
        public ActionResult Details(int? id)
        {
            // check if the id is null or not existing
            // I copied this code from Christine
            if ((id == null) || (database.Categories.Find(id) == null))
            {
                return HttpNotFound();
            }

            string detailsQuery = "SELECT * FROM Categories WHERE CategoryID = @catID";

            // create SQL Param object
            SqlParameter sqlValue = new SqlParameter("@catID", id);

            return View(database.Categories.SqlQuery(detailsQuery, sqlValue).FirstOrDefault());
        }
    }
}