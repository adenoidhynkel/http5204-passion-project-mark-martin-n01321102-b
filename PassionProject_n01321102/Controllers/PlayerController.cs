﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Net;
using PassionProject_n01321102.Models;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Data;
using System.IO;

namespace PassionProject_n01321102.Controllers
{
    public class PlayerController : Controller
    {
        private TeamBuilderContext database = new TeamBuilderContext();
        public ActionResult Index()
        {
            return RedirectToAction("List");
        }

        public ActionResult Register()
        {
            return View();
        }
        
        public ActionResult Profile(int id)
        {
            string query = "SELECT * FROM Players WHERE PlayerID = @playerid";
            return View(database.Players.SqlQuery(query, new SqlParameter("@playerid", id)).FirstOrDefault());
        }


        public ActionResult List()
        {
            return View(database.Players.ToList());
        }

        // Register an Account
        [HttpPost]
        public ActionResult Create(string New_PlayerFN, string New_PlayerLN, string New_PlayerDiscord)
        {
            string query = "INSERT INTO Players (PlayerFName, PlayerLName, PlayerDiscord, HasAvatar) " +
                "VALUES (@fname, @lname, @pldiscord, @hasAvatar)";

            // assign values to the sql parameter List
            // reference: https://stackoverflow.com/questions/425668/sql-parameter-collection/13676847
            // author: Drejc
            SqlParameter[] sqlValues = new SqlParameter[4]
            {
                new SqlParameter("@fname", SqlDbType.VarChar) { Value = New_PlayerFN},
                new SqlParameter("@lname", SqlDbType.VarChar) { Value =  New_PlayerLN},
                new SqlParameter("@pldiscord", SqlDbType.VarChar) { Value = New_PlayerDiscord},
                new SqlParameter("@hasAvatar", SqlDbType.Int) { Value = 0}
            };

            Debug.WriteLine(query);
            Debug.WriteLine(New_PlayerFN);
            Debug.WriteLine(New_PlayerLN);
            Debug.WriteLine(New_PlayerDiscord);
            Debug.WriteLine("after query");
            database.Database.ExecuteSqlCommand(query, sqlValues);
            return RedirectToAction("List");
        }

        public ActionResult Edit(int? id)
        {

            string query = "select * from Players where playerid = @id";
            return View(database.Players.SqlQuery(query, new SqlParameter("@id", id)).FirstOrDefault());

        }

        [HttpPost]
        public ActionResult Edit(int? id, string PlayerFName, string PlayerLName, string PlayerDiscord, HttpPostedFileBase PlayerAvatar)
        {
            string editPlayer = "update Players set PlayerFName = @fname, PlayerLName = @lname, PlayerDiscord = @pldiscord where PlayerID = @id";

            SqlParameter[] sqlValues = new SqlParameter[6] 
            {
                new SqlParameter("@fname", SqlDbType.VarChar) { Value = PlayerFName},
                new SqlParameter("@lname", SqlDbType.VarChar) { Value =  PlayerLName},
                new SqlParameter("@pldiscord", SqlDbType.VarChar) { Value = PlayerDiscord},
                new SqlParameter("@id", SqlDbType.Int) { Value = id},

                //these are just placeholders
                new SqlParameter("@playerAvatar", SqlDbType.VarChar) { Value = null},
                new SqlParameter("@hasAvatar", SqlDbType.Int) { Value = 0}
            };

            //TODO: Create image upload
            // I took this code from Christine's Example and modified it to suit my model
            if (PlayerAvatar.ContentLength > 0)
            {
                // modify the query
                editPlayer = "update Players set PlayerFName = @fname, PlayerLName = @lname, PlayerDiscord = @pldiscord, PlayerAvatar = @playerAvatar, HasAvatar = @hasAvatar where PlayerID = @id";

                // file extensioncheck taken from https://www.c-sharpcorner.com/article/file-upload-extension-validation-in-asp-net-mvc-and-javascript/
                // file  upload won't work without the attribute enctype="multipart/form-data"
                // source https://haacked.com/archive/2010/07/16/uploading-files-with-aspnetmvc.aspx/
                var valtypes = new[] { "jpeg", "jpg", "png", "gif" };
                var extension = Path.GetExtension(PlayerAvatar.FileName).Substring(1);

                if (valtypes.Contains(extension))
                {
                    //generic .img extension, web translates easily.
                    string image = id + "." + extension;

                    //get a direct file path to imgs/authors/
                    string path = Path.Combine(Server.MapPath("~/images/players"), image);

                    //save the file
                    PlayerAvatar.SaveAs(path);

                    // add the values to insert
                    sqlValues[4] = new SqlParameter("@playerAvatar", SqlDbType.VarChar) { Value = image };
                    sqlValues[5] = new SqlParameter("@hasAvatar", SqlDbType.Int) { Value = 1 };
                }
            }

            Debug.WriteLine(editPlayer);
            database.Database.ExecuteSqlCommand(editPlayer, sqlValues);

            return RedirectToAction("Profile/" + id);
        }

        public ActionResult Delete(int id)
        {
            //TODO: delete dependent tables form GuildxPlayer and PlayerXGames tables

            // Delete the player from the player table
            string query = "delete from Players where PlayerID = @id";

            database.Database.ExecuteSqlCommand(query, new SqlParameter("@id", id));

            return RedirectToAction("List");
        }

    }
}