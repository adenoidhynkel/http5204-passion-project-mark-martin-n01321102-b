﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Net;
using System.Data;
using System.Data.Entity;
using System.Data.SqlClient;
using PassionProject_n01321102.Models;
using PassionProject_n01321102.Models.MultiModels;
using System.Diagnostics;
using System.IO;


namespace PassionProject_n01321102.Controllers
{
    public class GuildController : Controller
    {
        TeamBuilderContext dbCTX = new TeamBuilderContext();

        // GET: Guild
        public ActionResult Index()
        {
            return RedirectToAction("List");
        }

        public ActionResult List()
        {
            string guildList = "SELECT * FROM Guilds";
            IEnumerable<Guild> guilds = dbCTX.Guilds.SqlQuery(guildList);
            // return query
            return View(guilds);
        }

        public ActionResult Add()
        {
            //string gameQuery = "SELECT * FROM Games";
            GuildMultiModel gameList = new GuildMultiModel();
            gameList.game = dbCTX.Games.ToList();
            return View(gameList);
        }

        public ActionResult Edit(int? id)
        {
            string editGuild = "SELECT * FROM Guilds WHERE GuildID = @guildId";
            GuildMultiModel editGuildMDL = new GuildMultiModel();
            editGuildMDL.guild = dbCTX.Guilds.SqlQuery(editGuild, new SqlParameter("@guildId", id)).FirstOrDefault();
            editGuildMDL.game = dbCTX.Games.ToList();
            return View(editGuildMDL);
        }

        [HttpPost]
        public ActionResult CreateGuild(string New_GuildName, string New_GuildDescription, int? New_gameList)
        {
            string addGuild = "INSERT INTO Guilds (GuildName, GuildDescription, game_GameID, HasEmblem) VALUES (@guildName, @guildDescription, @gameList, @hasEmblem)";

            //create a Sql Parameter Array
            SqlParameter[] sqlValues = new SqlParameter[4]
            {
                new SqlParameter("@guildName", SqlDbType.VarChar) { Value = New_GuildName },
                new SqlParameter("@guildDescription", SqlDbType.VarChar) { Value = New_GuildDescription },
                new SqlParameter("@gameList", SqlDbType.Int) { Value = New_gameList },
                new SqlParameter("@hasEmblem", SqlDbType.Int) { Value = 0 }
            };

            dbCTX.Database.ExecuteSqlCommand(addGuild, sqlValues);

            return RedirectToAction("List");
        }

        [HttpPost]
        public ActionResult UpdateGuild(int? id, string GuildName, string GuildDescription, HttpPostedFileBase GuildEmblem)
        {
            string editGuild = "UPDATE Guilds SET GuildName = @GuildName, GuildDescription = @guildDesc WHERE GuildId = @guildId";


            // create a Sql Parameter Array
            SqlParameter[] sqlValues = new SqlParameter[5]
            {
                new SqlParameter("@guildName", SqlDbType.VarChar) { Value = GuildName },
                new SqlParameter("@guildDesc", SqlDbType.VarChar) { Value = GuildDescription },
                new SqlParameter("@guildId", SqlDbType.Int) { Value = id },

                // filler values
                 new SqlParameter("@guildEmblem", SqlDbType.VarChar) { Value = null },
                 new SqlParameter("@hasEmblem", SqlDbType.Int) { Value = 0 }
        };

            //TODO: Create image upload
            // I took this code from Christine's Example and modified it to suit my model
            if (GuildEmblem.ContentLength > 0)
            {
                // modify the query
                editGuild = "UPDATE Guilds SET GuildName = @GuildName, GuildDescription = @guildDesc, GuildEmblem = @guildEmblem, HasEmblem = @hasEmblem WHERE GuildId = @guildId";

                // file extensioncheck taken from https://www.c-sharpcorner.com/article/file-upload-extension-validation-in-asp-net-mvc-and-javascript/
                // file  upload won't work without the attribute enctype="multipart/form-data"
                // source https://haacked.com/archive/2010/07/16/uploading-files-with-aspnetmvc.aspx/
                var valtypes = new[] { "jpeg", "jpg", "png", "gif" };
                var extension = Path.GetExtension(GuildEmblem.FileName).Substring(1);

                if (valtypes.Contains(extension))
                {
                    //generic .img extension, web translates easily.
                    string emblem = id + "." + extension;

                    //get a direct file path to imgs/authors/
                    string path = Path.Combine(Server.MapPath("~/images/guilds"), emblem);

                    //save the file
                    GuildEmblem.SaveAs(path);

                    // add the values to insert
                    sqlValues[3] = new SqlParameter("@guildEmblem", SqlDbType.VarChar) { Value = emblem };
                    sqlValues[4] = new SqlParameter("@hasEmblem", SqlDbType.Int) { Value = 1 };
                }
            }

            dbCTX.Database.ExecuteSqlCommand(editGuild, sqlValues);

            // redirect to the game detail page
            return RedirectToAction("Summary/" + id);
        }

        public ActionResult Summary(int? id)
        {
            string guildSummary = "SELECT * FROM Guilds WHERE GuildID = @guildId";

            // return result

            return View(dbCTX.Guilds.SqlQuery(guildSummary, new SqlParameter("@guildId", id)).FirstOrDefault());
        }

        public ActionResult Delete(int? id)
        {
            string deleteGuild = "DELETE FROM Guilds WHERE GuildID = @guildId";

            dbCTX.Database.ExecuteSqlCommand(deleteGuild, new SqlParameter("@guildId", id));

            // return to game list
            return RedirectToAction("List");
        }
    }
}