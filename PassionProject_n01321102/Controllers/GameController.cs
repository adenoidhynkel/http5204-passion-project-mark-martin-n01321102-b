﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Net;
using System.Data;
using System.Data.Entity;
using System.Data.SqlClient;
using PassionProject_n01321102.Models;
using PassionProject_n01321102.Models.MultiModels;
using System.Diagnostics;
using System.IO;

namespace PassionProject_n01321102.Controllers
{
    public class GameController : Controller
    {
        //database object
        TeamBuilderContext dbContext = new TeamBuilderContext();
        // GET: Game
        public ActionResult Index()
        {
            return RedirectToAction("List");
        }

        public ActionResult List()
        {
            string gameList = "SELECT * FROM GAMES";
            IEnumerable<Game> games = dbContext.Games.SqlQuery(gameList);
            // return query
            return View(games);
            
        }

        public ActionResult Add()
        {
            GameMultiModel categories = new GameMultiModel();
            categories.Categories = dbContext.Categories.ToList();
            return View(categories);
        }

        public ActionResult Edit(int? id)
        {
            string editGame = "SELECT * FROM Games WHERE GameID = @gameid";
            GameMultiModel games = new GameMultiModel();
            games.Games = dbContext.Games.SqlQuery(editGame, new SqlParameter("@gameid", id)).FirstOrDefault();
            games.Categories = dbContext.Categories.ToList();
            return View(games);
        }

        [HttpPost]
        public ActionResult AddGame(string New_GameName, string New_GameDescription, int? New_GameCategory)
        {
            string addGame = "INSERT INTO Games (GameName, GameDescription, HasLogo) VALUES (@gameName, @gameDescription, @hasLogo)";

            //create a Sql Parameter Array
            SqlParameter[] sqlValues = new SqlParameter[3]
            {
                new SqlParameter("@gameName", SqlDbType.VarChar) { Value = New_GameName },
                new SqlParameter("@gameDescription", SqlDbType.VarChar) { Value = New_GameDescription },
                new SqlParameter("@hasLogo", SqlDbType.Int) { Value = 0 },
                //new SqlParameter("@gameCID", SqlDbType.Int) {Value = New_GameCategory}
            };

            dbContext.Database.ExecuteSqlCommand(addGame, sqlValues);

            
            // get the max ID after adding to database
            // source: https://stackoverflow.com/questions/315946/how-do-i-get-the-max-id-with-linq-to-entity
            // author: Thiago Valente
            int? New_GameID = dbContext.Games.Max(g => (int?)g.GameID);
            //Debug.WriteLine(newGameID);

            string addCategory = "INSERT INTO CategoryGames (Category_CategoryID, Game_GameID) VALUES (@gameCatID, @gameID)";
            SqlParameter[] categories = new SqlParameter[2] 
            {
                new SqlParameter("@gameID", SqlDbType.Int) {Value = New_GameID },
                new SqlParameter("@gameCatID", SqlDbType.Int) {Value = New_GameCategory}
            };

            // run the query
            dbContext.Database.ExecuteSqlCommand(addCategory, categories);

            return RedirectToAction("List");
        }

        [HttpPost]
        public ActionResult Update(int? id, string GameName, string GameDescription, int gameCategory, HttpPostedFileBase GameLogo)
        {
            string editGame = "UPDATE Games SET GameName = @gameName, GameDescription = @gameDesc WHERE GameID = @gameid";

            // create a Sql Parameter Array
            SqlParameter[] sqlValues = new SqlParameter[5]
            {
                new SqlParameter("@gameName", SqlDbType.VarChar) { Value = GameName },
                new SqlParameter("@gameDesc", SqlDbType.VarChar) { Value = GameDescription },
                new SqlParameter("@gameid", SqlDbType.Int) { Value = id },

                // filler values this will be modified if user uploads an image
                new SqlParameter("@gameLogo", SqlDbType.VarChar) { Value = null },
                new SqlParameter("@hasLogo", SqlDbType.Int) { Value = 0 }
            };

            // TODO: Add game logo upload
            // I took this code from Christine's Example and modified it to suit my model

            // check if there is a posted file
            if (GameLogo != null)
            {
                // check the posted file
                if (GameLogo.ContentLength > 0)
                {
                    // modify the query
                    editGame = "UPDATE Games SET GameName = @gameName, GameDescription = @gameDesc, GameLogo = @gameLogo, HasLogo = @hasLogo WHERE GameID = @gameid";

                    //file extensioncheck taken from https://www.c-sharpcorner.com/article/file-upload-extension-validation-in-asp-net-mvc-and-javascript/
                    var valtypes = new[] { "jpeg", "jpg", "png", "gif" };
                    var extension = Path.GetExtension(GameLogo.FileName).Substring(1);

                    if (valtypes.Contains(extension))
                    {
                        //generic .img extension, web translates easily.
                        string logo = id + "." + extension;

                        //get a direct file path to imgs/authors/
                        string path = Path.Combine(Server.MapPath("~/images/games"), logo);

                        //save the file
                        GameLogo.SaveAs(path);

                        // add the values to insert
                        sqlValues[3] = new SqlParameter("@gameLogo", SqlDbType.VarChar) { Value = logo };
                        sqlValues[4] = new SqlParameter("@hasLogo", SqlDbType.Int) { Value = 1 };
                    }
                }
            }
            else
            {
                // splice the array if there is no posted file
                // code based from https://asp-net-example.blogspot.com/2013/10/c-example-array-remove-last-element.html
                // author: Saiful Alam
                Array.Resize(ref sqlValues, sqlValues.Length - 2);
            }

            dbContext.Database.ExecuteSqlCommand(editGame, sqlValues);

            // add to CategoryxGames Table
            string categoryQuery = "UPDATE CategoryGames SET Category_CategoryID = @gameCatID WHERE Game_GameID = @gameID";
            SqlParameter[] catValues = new SqlParameter[2]
            {
                new SqlParameter("@gameCatID", SqlDbType.Int) { Value = gameCategory },
                new SqlParameter("@gameID", SqlDbType.Int) { Value = id }
            };

            dbContext.Database.ExecuteSqlCommand(categoryQuery, catValues);

            // redirect to the game detail page
            return RedirectToAction("Summary/" + id);
        }

        public ActionResult Summary(int? id)
        {
            string gameSummary = "SELECT * FROM GAMES WHERE GameID = @gameid";

            // return result

            return View(dbContext.Games.SqlQuery(gameSummary,new SqlParameter("@gameid", id)).FirstOrDefault());
        }

        public ActionResult Delete(int? id)
        {
            string deleteGame = "DELETE FROM Games WHERE GameID = @gameid";

            dbContext.Database.ExecuteSqlCommand(deleteGame, new SqlParameter("@gameid", id));

            // return to game list
            return RedirectToAction("List");
        }
    }
}