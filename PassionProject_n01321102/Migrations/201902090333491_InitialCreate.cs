namespace PassionProject_n01321102.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Applications",
                c => new
                    {
                        ApplicationId = c.Int(nullable: false, identity: true),
                        ApplicationDate = c.DateTime(nullable: false, storeType: "date"),
                    })
                .PrimaryKey(t => t.ApplicationId);
            
            CreateTable(
                "dbo.Guilds",
                c => new
                    {
                        GuildId = c.Int(nullable: false, identity: true),
                        GuildName = c.String(nullable: false, maxLength: 255),
                        GuildDescription = c.String(maxLength: 255),
                        HasEmblem = c.Int(nullable: false),
                        GuildEmblem = c.String(maxLength: 255),
                        game_GameID = c.Int(),
                    })
                .PrimaryKey(t => t.GuildId)
                .ForeignKey("dbo.Games", t => t.game_GameID)
                .Index(t => t.game_GameID);
            
            CreateTable(
                "dbo.Games",
                c => new
                    {
                        GameID = c.Int(nullable: false, identity: true),
                        GameName = c.String(nullable: false, maxLength: 255),
                        GameDescription = c.String(maxLength: 255),
                        HasLogo = c.Int(nullable: false),
                        GameLogo = c.String(maxLength: 255),
                        Player_PlayerID = c.Int(),
                    })
                .PrimaryKey(t => t.GameID)
                .ForeignKey("dbo.Players", t => t.Player_PlayerID)
                .Index(t => t.Player_PlayerID);
            
            CreateTable(
                "dbo.Categories",
                c => new
                    {
                        CategoryID = c.Int(nullable: false, identity: true),
                        CategoryName = c.String(nullable: false, maxLength: 255),
                        CategoryDescription = c.String(maxLength: 255),
                    })
                .PrimaryKey(t => t.CategoryID);
            
            CreateTable(
                "dbo.Players",
                c => new
                    {
                        PlayerID = c.Int(nullable: false, identity: true),
                        PlayerFName = c.String(nullable: false, maxLength: 255),
                        PlayerLName = c.String(nullable: false, maxLength: 255),
                        PlayerDiscord = c.String(maxLength: 255),
                        HasAvatar = c.Int(nullable: false),
                        PlayerAvatar = c.String(maxLength: 255),
                        Application_ApplicationId = c.Int(),
                    })
                .PrimaryKey(t => t.PlayerID)
                .ForeignKey("dbo.Applications", t => t.Application_ApplicationId)
                .Index(t => t.Application_ApplicationId);
            
            CreateTable(
                "dbo.GuildApplications",
                c => new
                    {
                        Guild_GuildId = c.Int(nullable: false),
                        Application_ApplicationId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Guild_GuildId, t.Application_ApplicationId })
                .ForeignKey("dbo.Guilds", t => t.Guild_GuildId, cascadeDelete: true)
                .ForeignKey("dbo.Applications", t => t.Application_ApplicationId, cascadeDelete: true)
                .Index(t => t.Guild_GuildId)
                .Index(t => t.Application_ApplicationId);
            
            CreateTable(
                "dbo.CategoryGames",
                c => new
                    {
                        Category_CategoryID = c.Int(nullable: false),
                        Game_GameID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Category_CategoryID, t.Game_GameID })
                .ForeignKey("dbo.Categories", t => t.Category_CategoryID, cascadeDelete: true)
                .ForeignKey("dbo.Games", t => t.Game_GameID, cascadeDelete: true)
                .Index(t => t.Category_CategoryID)
                .Index(t => t.Game_GameID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Players", "Application_ApplicationId", "dbo.Applications");
            DropForeignKey("dbo.Games", "Player_PlayerID", "dbo.Players");
            DropForeignKey("dbo.Guilds", "game_GameID", "dbo.Games");
            DropForeignKey("dbo.CategoryGames", "Game_GameID", "dbo.Games");
            DropForeignKey("dbo.CategoryGames", "Category_CategoryID", "dbo.Categories");
            DropForeignKey("dbo.GuildApplications", "Application_ApplicationId", "dbo.Applications");
            DropForeignKey("dbo.GuildApplications", "Guild_GuildId", "dbo.Guilds");
            DropIndex("dbo.CategoryGames", new[] { "Game_GameID" });
            DropIndex("dbo.CategoryGames", new[] { "Category_CategoryID" });
            DropIndex("dbo.GuildApplications", new[] { "Application_ApplicationId" });
            DropIndex("dbo.GuildApplications", new[] { "Guild_GuildId" });
            DropIndex("dbo.Players", new[] { "Application_ApplicationId" });
            DropIndex("dbo.Games", new[] { "Player_PlayerID" });
            DropIndex("dbo.Guilds", new[] { "game_GameID" });
            DropTable("dbo.CategoryGames");
            DropTable("dbo.GuildApplications");
            DropTable("dbo.Players");
            DropTable("dbo.Categories");
            DropTable("dbo.Games");
            DropTable("dbo.Guilds");
            DropTable("dbo.Applications");
        }
    }
}
