﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PassionProject_n01321102.Models.MultiModels
{
    public class GameMultiModel
    {
        public GameMultiModel()
        {

        }

        public virtual Game Games { get; set; }
        public IEnumerable<Category> Categories { get; set; }
    }
}