﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PassionProject_n01321102.Models.MultiModels
{
    public class GuildMultiModel
    {
        public GuildMultiModel()
        {

        }

        public virtual Guild guild { get; set; }
        public IEnumerable<Game> game { get; set; }
        public IEnumerable<Application> application { get; set; }
    }
}