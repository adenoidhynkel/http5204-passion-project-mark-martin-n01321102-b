﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace PassionProject_n01321102.Models
{
    public class TeamBuilderContext : DbContext
    {
        public TeamBuilderContext()
        {

        }

        public DbSet<Player> Players { get; set; }
        public DbSet<Application> Applications { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<Guild> Guilds { get; set; }
        public DbSet<Game> Games { get; set; }
    }
}