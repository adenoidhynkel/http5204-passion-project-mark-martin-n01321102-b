﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace PassionProject_n01321102.Models
{
    public class Category
    {
        [Key, ScaffoldColumn(false)]
        public int CategoryID { get; set; }

        [Required, StringLength(255), Display(Name = "Category Name")]
        public string CategoryName { get; set; }

        [StringLength(255), Display(Name = "Description")]
        public string CategoryDescription { get; set; }

        public virtual ICollection<Game> Games { get; set; }
    }
}