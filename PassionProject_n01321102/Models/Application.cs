﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PassionProject_n01321102.Models
{
    public class Application
    {
        [Key, ScaffoldColumn(false)]
        public int ApplicationId { get; set; }


        // Application Date 
        // got this code from stackoverflow
        // it helps me declare a date data type
        // date accessed: feb 2 2019
        // author: Ladislav Mrnka
        // source: https://stackoverflow.com/questions/5658216/entity-framework-code-first-date-field-creation

        [Column(TypeName = "Date")]
        public DateTime ApplicationDate { get; set; }

        public virtual ICollection<Guild> Guild { get; set; }
        public virtual ICollection<Player> Player { get; set; }

        // TODO : add a flag if accepted or rejected
    }
}